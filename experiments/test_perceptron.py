import os

from data.data import *
from implementation.linear_classifier import LinearClassifier

weights_ranges = [(-1, 1), (-0.8, 0.8), (-0.6, 0.6), (-0.4, 0.4), (-0.2, 0.2), (0.0, 0.0)]
alpha_range = [0.0001, 0.001, 0.01, 0.1, 1]

VERBOSE = False
VERBOSE_PREDICT = True


def run_tests():

    try:
        os.remove(os.getcwd() + '/data/output.txt')
    except FileNotFoundError:
        pass

    # unipolar or
    for weight_range in weights_ranges:
        for number in range(1, 16):
            perceptron = LinearClassifier(activation_type='unipolar', weights_range=weight_range, verbose=VERBOSE, label='or-' + str(number))
            perceptron.train(up_or[0], up_or[1])
            # perceptron.predict(up_test, verbose=VERBOSE_PREDICT)

    for alpha in alpha_range:
        perceptron = LinearClassifier(alpha=alpha, activation_type='unipolar', verbose=VERBOSE, label='or')
        perceptron.train(up_or[0], up_or[1])
        # perceptron.predict(up_test, verbose=VERBOSE_PREDICT)

    # unipolar and
    for weight_range in weights_ranges:
        for number in range(1, 16):
            perceptron = LinearClassifier(activation_type='unipolar', weights_range=weight_range, verbose=VERBOSE, label='and-' + str(number))
            perceptron.train(up_and[0], up_and[1])
            # perceptron.predict(up_test, verbose=VERBOSE_PREDICT)

    for alpha in alpha_range:
        perceptron = LinearClassifier(alpha=alpha, activation_type='unipolar', verbose=VERBOSE, label='and')
        perceptron.train(up_and[0], up_and[1])
        # perceptron.predict(up_test, verbose=VERBOSE_PREDICT)

    # bipolar or
    for weight_range in weights_ranges:
        for number in range(1, 16):
            perceptron = LinearClassifier(weights_range=weight_range, verbose=VERBOSE, label='or-' + str(number))
            perceptron.train(bp_or[0], bp_or[1])
            # perceptron.predict(bp_test, verbose=VERBOSE_PREDICT)

    for alpha in alpha_range:
        perceptron = LinearClassifier(alpha=alpha, verbose=VERBOSE, label='or')
        perceptron.train(bp_or[0], bp_or[1])
        # perceptron.predict(bp_test, verbose=VERBOSE_PREDICT)

    # bipolar and
    for weight_range in weights_ranges:
        for number in range(1, 16):
            perceptron = LinearClassifier(weights_range=weight_range, verbose=VERBOSE, label='and-' + str(number))
            perceptron.train(bp_and[0], bp_and[1])
            # perceptron.predict(bp_test, verbose=VERBOSE_PREDICT)

    for alpha in alpha_range:
        perceptron = LinearClassifier(alpha=alpha, verbose=VERBOSE, label='and')
        perceptron.train(bp_and[0], bp_and[1])
        # perceptron.predict(bp_test, verbose=VERBOSE_PREDICT)

