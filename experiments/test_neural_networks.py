from experiments.test_neural_network import run_test
from implementation.neural_network import NeuralNetwork

DEF_LAYERS_SIZES = [784, 90, 10]
DEF_ETA = 0.01
DEF_BETA = 0.3
DEF_BATCH = 40
DEF_INITIALIZER = 'xavier'
DEF_ACTIVATION = 'sigmoid'
DEF_LOSS = 'mean_squared'
DEF_EPOCHS = 100
DEF_OPTIMIZER = 'none'
DEF_VERBOSE = True


def run():
    # optimizers

    # network = NeuralNetwork(DEF_LAYERS_SIZES)
    # run_test(network)
    #
    # network = NeuralNetwork(layers=DEF_LAYERS_SIZES, eta=DEF_ETA, beta=DEF_BETA, batch_size=DEF_BATCH, initializer=DEF_INITIALIZER,
    #                         activation=DEF_ACTIVATION, loss=DEF_LOSS, epochs=DEF_EPOCHS, training_set_prediction=False,
    #                         verbose=DEF_VERBOSE, label='optimizer_none', optimizer='none')
    # run_test(network)
    #
    # network = NeuralNetwork(layers=DEF_LAYERS_SIZES, eta=DEF_ETA, beta=0.1, batch_size=DEF_BATCH,
    #                         initializer=DEF_INITIALIZER,
    #                         activation=DEF_ACTIVATION, loss=DEF_LOSS, epochs=DEF_EPOCHS, training_set_prediction=False,
    #                         verbose=DEF_VERBOSE, label='optimizer_momentum_01', optimizer='momentum')
    # run_test(network)
    #
    # network = NeuralNetwork(layers=DEF_LAYERS_SIZES, eta=DEF_ETA, beta=0.3, batch_size=DEF_BATCH,
    #                         initializer=DEF_INITIALIZER,
    #                         activation=DEF_ACTIVATION, loss=DEF_LOSS, epochs=DEF_EPOCHS, training_set_prediction=False,
    #                         verbose=DEF_VERBOSE, label='optimizer_momentum_03', optimizer='momentum')
    # run_test(network)
    #
    # network = NeuralNetwork(layers=DEF_LAYERS_SIZES, eta=DEF_ETA, beta=0.5, batch_size=DEF_BATCH,
    #                         initializer=DEF_INITIALIZER,
    #                         activation=DEF_ACTIVATION, loss=DEF_LOSS, epochs=DEF_EPOCHS, training_set_prediction=False,
    #                         verbose=DEF_VERBOSE, label='optimizer_momentum_05', optimizer='momentum')
    # run_test(network)
    #
    # network = NeuralNetwork(layers=DEF_LAYERS_SIZES, eta=DEF_ETA, beta=DEF_BETA, batch_size=DEF_BATCH, initializer=DEF_INITIALIZER,
    #                         activation=DEF_ACTIVATION, loss=DEF_LOSS, epochs=DEF_EPOCHS, training_set_prediction=False,
    #                         verbose=DEF_VERBOSE, label='optimizer_adagrad', optimizer='adagrad')
    # run_test(network)
    #
    # network = NeuralNetwork(layers=DEF_LAYERS_SIZES, eta=DEF_ETA, beta=DEF_BETA, batch_size=DEF_BATCH, initializer=DEF_INITIALIZER,
    #                         activation=DEF_ACTIVATION, loss=DEF_LOSS, epochs=DEF_EPOCHS, training_set_prediction=False,
    #                         verbose=DEF_VERBOSE, label='optimizer_adadelta', optimizer='adadelta')
    # run_test(network)
    #
    # network = NeuralNetwork(layers=DEF_LAYERS_SIZES, eta=DEF_ETA, beta=DEF_BETA, batch_size=DEF_BATCH, initializer=DEF_INITIALIZER,
    #                         activation=DEF_ACTIVATION, loss=DEF_LOSS, epochs=DEF_EPOCHS, training_set_prediction=False,
    #                         verbose=DEF_VERBOSE, label='optimizer_adam', optimizer='adam')
    # run_test(network)

    # loss
    #
    # network = NeuralNetwork(layers=DEF_LAYERS_SIZES, eta=DEF_ETA, beta=DEF_BETA, batch_size=DEF_BATCH, initializer=DEF_INITIALIZER,
    #                         activation='sigmoid', loss='cross_entropy', epochs=DEF_EPOCHS, training_set_prediction=False,
    #                         verbose=DEF_VERBOSE, label='sigmoid_cross_entropy')
    # run_test(network)

    network = NeuralNetwork(layers=DEF_LAYERS_SIZES, eta=DEF_ETA, beta=DEF_BETA, batch_size=DEF_BATCH, initializer=DEF_INITIALIZER,
                            activation='sigmoid', loss='cross_entropy', epochs=DEF_EPOCHS, training_set_prediction=False,
                            verbose=DEF_VERBOSE, label='softmax_cross_entropy')
    run_test(network)
