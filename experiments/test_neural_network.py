import numpy as np

from data import mnist_loader


def run_test(network):
    training_data, validation_data, test_data = mnist_loader.load()

    samples, labels = zip(*training_data)
    samples = np.squeeze(samples)
    labels = np.squeeze(labels)

    validation_samples, validation_labels = zip(*validation_data)
    validation_samples = np.squeeze(validation_samples)
    validation_labels = np.reshape(np.squeeze(validation_labels), (10000, 1))
    val = (validation_samples, validation_labels)

    test_samples, test_labels = zip(*test_data)
    test_samples = np.squeeze(test_samples)
    test_labels = np.reshape(np.squeeze(test_labels), (10000, 1))

    network.train(samples, labels, val)
    prediction = network.predict(test_samples, parameters='best')
    result = network.check_prediction(prediction, test_labels)
    print(f'Test set accuracy: {result * 100}% for {network.label}')
