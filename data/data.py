import numpy as np

x_bp = np.array(
    [
        [-1, -1],
        [-1, 1],
        [1, -1],
        [1, 1]
    ]
)

x_up = np.array(
    [
        [0, 0],
        [0, 1],
        [1, 0],
        [1, 1]
    ]
)


y_bp_or = np.array(
    [
        [-1],
        [1],
        [1],
        [1]
    ]
)

y_up_or = np.array(
    [
        [0],
        [1],
        [1],
        [1]
    ]
)

y_bp_and = np.array(
    [
        [-1],
        [-1],
        [-1],
        [1]
    ]
)

y_up_and = np.array(
    [
        [0],
        [0],
        [0],
        [1]
    ]
)

bp_test = np.array(
    [
        [-1, -1],
        [-1, 1],
        [1, -1],
        [1, 1]
    ]
)


up_test = np.array(
    [
        [0, 0],
        [0, 1],
        [1, 0],
        [1, 1]
    ]
)

up_or = (x_up, y_up_or)
up_and = (x_up, y_up_and)
bp_or = (x_bp, y_bp_or)
bp_and = (x_bp, y_bp_and)