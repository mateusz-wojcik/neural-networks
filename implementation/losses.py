import numpy as np


def mean_squared(y_pred, y):
    # return 0.5 * np.linalg.norm(error) ** 2
    error = y_pred - y
    return np.mean(np.power(error, 2))


def cross_entropy(y_pred, y):
    return -(y * np.log(y_pred))
    # return -(y * np.log(y_pred) + (1 - y) * np.log(1 - y_pred))
