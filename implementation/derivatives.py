import numpy as np
from implementation.activations import sigmoid

# activation


def dv_sigmoid(z):
    return sigmoid(z) * (1 - sigmoid(z))


def dv_relu(z):
    return np.where(z > 0, 1, 0)


def dv_softmax(z):
    return 1


# loss


def dv_mean_squared(y_pred, y):
    return y_pred - y


def dv_cross_entropy(y_pred, y):
    return y_pred - y
    # return (-y / y_pred) + ((1 - y) / (1 - y_pred))
