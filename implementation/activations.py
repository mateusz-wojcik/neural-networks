import numpy as np


def unipolar(z):
    return z > 0


def bipolar(z):
    result = (z > 0).astype(int)
    result[result == 0] = -1
    return result


def sigmoid(z):
    return 1 / (1 + np.exp(-z))


def relu(z):
    return np.where(z > 0, z, 0)


def softmax(z):
    z = np.transpose(z)
    exps = np.exp(z - np.max(z))
    return (exps / np.sum(exps, axis=1, keepdims=True)).T
