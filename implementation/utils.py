import matplotlib.pyplot as plt
from collections import Counter
import numpy as np


def show_dataset_samples(labels):
    labels_normalized = [np.argmax(label) for label in labels]
    cnt = Counter(labels_normalized)
    plt.bar(cnt.keys(), cnt.values())
    plt.xlabel('Cyfra')
    plt.ylabel('Liczba wystąpień')
    plt.title('Cyfra a liczba jej wystąpień w zbiorze treningowym')
    plt.xticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    plt.show()
