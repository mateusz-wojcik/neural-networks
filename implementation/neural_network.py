import random

import numpy as np

import implementation.activations as act
import implementation.derivatives as der
import implementation.initializers as ini
import implementation.losses as err
import implementation.optimizers as opt


class NeuralNetwork:
    def __init__(self, layers, eta=0.01, beta=0.3, batch_size=40, initializer='xavier', activation='sigmoid',
                 loss='cross_entropy', epochs=100, training_set_prediction=False, verbose=True, label=None,
                 optimizer='none', epsilon=1e-8, beta1=0.9, beta2=0.999):
        self.layers_count = len(layers)
        self.layers = layers
        self.eta = eta
        self.beta = beta
        self.batch_size = batch_size
        self.initializer = getattr(ini, initializer)
        self.weights = [self.initializer(size1, size2) for size1, size2 in zip(layers[1:], layers[:-1])]
        self.biases = [np.zeros((size, 1)) for size in layers[1:]]
        self.activation = getattr(act, activation)
        self.loss = getattr(err, loss)
        self.derivative = getattr(der, 'dv_' + activation)
        self.cost_derivative = getattr(der, 'dv_' + loss)
        self.epochs = epochs
        self.training_set_prediction = training_set_prediction
        self.verbose = verbose
        self.label = label
        self.optimizer = getattr(opt, optimizer)
        self.epsilon = epsilon
        self.beta1 = beta1
        self.beta2 = beta2
        self.m = [np.zeros((size1, size2)) for size1, size2 in zip(layers[1:], layers[:-1])]
        self.v = [np.zeros((size1, size2)) for size1, size2 in zip(layers[1:], layers[:-1])]
        self.activations = []
        self.z = []
        self.prev_delta_w = [0] * (self.layers_count - 1)
        self.squared_gradients = [np.zeros((size1, size2)) for size1, size2 in zip(layers[1:], layers[:-1])]
        self.squared_updates = [np.zeros((size1, size2)) for size1, size2 in zip(layers[1:], layers[:-1])]
        self.best_acc = 0
        self.best_weights = None
        self.best_biases = None
        self.adadelta_helper = 0

    def forward(self, a, parameters='normal'):
        a = np.transpose(np.atleast_2d(a))
        self.activations, self.z = [], []
        self.activations.append(a)
        w, b = (self.weights, self.biases) if parameters == 'normal' else (self.best_weights, self.best_biases)
        i = 0
        for weights, biases in zip(w, b):
            i += 1
            z = np.dot(weights, a) + biases
            a = self.activation(z) if i != 2 else getattr(act, 'softmax')(z)
            self.z.append(z)
            self.activations.append(a)
        return a

    def backpropagation(self, y_pred, y):
        delta = self.cost_derivative(y_pred, y) # * self.derivative(self.z[-1])
        update, update_bias = self.optimizer(self, delta, -1)

        for i in range(self.layers_count - 2, -1, -1):
            self.weights[i] = self.weights[i] - update
            self.biases[i] = self.biases[i] - update_bias
            if i != 0:
                delta = self.weights[i].T @ delta * self.derivative(self.z[i - 1])
                update, update_bias = self.optimizer(self, delta, i - 1)

    def train(self, x, y, validation_data=None):
        x_y = list(zip(x, y))
        n = len(x_y)
        costs, train_acc, val_acc, epochs = [], [], [], []

        for epoch in range(self.epochs):
            self.adadelta_helper = epoch + 1
            losses = []
            random.shuffle(x_y)
            x, y = zip(*x_y)

            for k in range(0, n, self.batch_size):
                x_batch, y_batch = x[k:k + self.batch_size], np.transpose(np.atleast_2d(y[k:k + self.batch_size]))
                a = self.forward(x_batch)
                self.backpropagation(a, y_batch)
                losses.append(self.loss(a, y_batch))

            train_accuracy, val_accuracy = None, None
            if validation_data is not None:
                prediction, labels = self.predict(validation_data[0]), validation_data[1]
                val_accuracy = self.check_prediction(prediction, labels) * 100
                if val_accuracy > self.best_acc:
                    self.best_acc = val_accuracy
                    self.best_weights, self.best_biases = np.copy(self.weights), np.copy(self.biases)

            if self.training_set_prediction:
                train_prediction, train_labels = self.predict(x), np.argmax(y, axis=0)
                train_accuracy = self.check_prediction(train_prediction, train_labels) * 100

            cost = self.cost(losses)
            epochs.append(epoch + 1)
            costs.append(cost)
            train_acc.append(train_accuracy)
            val_acc.append(val_accuracy)

            if self.verbose and epoch % 20 == 0:
                print(f'Epoch {epoch + 1}   cost: {cost}   train accuracy: {train_accuracy}%   val acuracy: {val_accuracy}%')

        if self.label is not None:
            np.savetxt('data_' + self.label + '.csv', [p for p in zip(epochs, costs, train_acc, val_acc)], delimiter=';', fmt='%s')
        self.adadelta_helper = 0

    def predict(self, x, parameters='normal'):
        return [self.__predict_one(data, parameters=parameters) for data in x]

    def __predict_one(self, x, parameters='normal'):
        a = self.forward(x, parameters=parameters)
        return [np.argmax(prediction) for prediction in a.T]

    @staticmethod
    def check_prediction(y_pred, output):
        return np.sum(y_pred == output) / len(y_pred)

    @staticmethod
    def cost(losses):
        return np.mean(losses)
