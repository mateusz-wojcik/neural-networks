import os

import numpy as np
from implementation.activations import unipolar, bipolar


class LinearClassifier:
    def __init__(self, alpha=0.01, epochs=10000, weights=None, weights_range=(-1, 1), activation_type='bipolar',
                 mode='perceptron', error_tolerance=0.1, verbose=True, log_results=True, label=''):
        self.alpha = alpha
        self.epochs = epochs
        self.weights_range = weights_range
        self.w = weights if weights is not None else self.__get_random_weights()
        self.activation_type = activation_type
        self.mode = mode
        self.error_tolerance = error_tolerance
        self.verbose = verbose
        self.log_results = log_results
        self.label = label
        self.b = 0
        self.report_data = self.__init_report_data()

    def stimulation(self, x):
        return x @ self.w + self.b

    def activation(self, z):
        if self.activation_type == 'bipolar':
            return bipolar(z)
        elif self.activation_type == 'unipolar':
            return unipolar(z)
        raise Exception('Incorrect mode function name')

    def train(self, x, y):
        for epoch in range(self.epochs):
            error = self.error(y, x)
            self.w = self.w + self.alpha * (error.T @ x).T
            self.b = self.b + self.alpha * self.loss(error, 'perceptron')

            if (self.mode == 'adaline' and self.loss(error) < self.error_tolerance)\
                    or (self.mode == 'perceptron' and (np.sum(error) == 0)):
                break

            if self.verbose:
                print(f'Epoch: {epoch + 1}: loss {self.loss(error)} bias {self.b} weights \n {self.w}')

        if self.log_results:
            self.__log_results([str(epoch + 1), str(self.loss(error))])

    def predict(self, test_data, verbose=True):
        result = np.concatenate(list(map(self.activation, self.stimulation(test_data))))
        if verbose:
            self.__print_prediction(test_data, result)
        return result

    def error(self, y, x):
        if self.mode == 'perceptron':
            return y - self.activation(self.stimulation(x))
        elif self.mode == 'adaline':
            return y - self.stimulation(x)
        raise Exception('Incorrect mode function name')

    def loss(self, error, mode=None):
        mode = self.mode if mode is None else mode
        if mode == 'perceptron':
            return np.sum(error)
        elif mode == 'adaline':
            return np.mean(error ** 2)
        raise Exception('Incorrect mode name')

    def __init_report_data(self):
        # type(and/or), polar, alpha, weights_range, mode, error_tolerance, bias, end_epoch, error
        return [self.label, self.activation_type, str(self.alpha), str(self.weights_range),
                self.mode, str(self.error_tolerance), str(self.b)]

    def __get_report_string(self):
        return ';'.join(self.report_data) + '\n'

    def __get_random_weights(self):
        return np.random.uniform(self.weights_range[0], self.weights_range[1], (2, 1))

    def __log_results(self, extras):
        self.report_data.extend(extras)
        with open(os.getcwd() + '/data/output.txt', 'a') as output_file:
            output_file.write(self.__get_report_string())

    @staticmethod
    def __print_prediction(test_data, result):
        print('Prediction results:')
        for i in range(len(test_data)):
            print(str(test_data[i]) + ' => ' + str(int(result[i])))
