import numpy as np


def none(network, delta, index):
    gradient = (delta @ network.activations[-2 if index == -1 else index].T)
    update = network.eta * gradient
    update_bias = network.eta * np.reshape(np.sum(delta, axis=1), network.biases[index].shape)
    return update, update_bias


def momentum(network, delta, index):
    gradient = (delta @ network.activations[-2 if index == -1 else index].T)
    update = network.eta * gradient + network.beta * network.prev_delta_w[index]
    update_bias = network.eta * np.reshape(np.sum(delta, axis=1), network.biases[index].shape)
    network.prev_delta_w[index] = update
    return update, update_bias


def adagrad(network, delta, index):
    gradient = (delta @ network.activations[-2 if index == -1 else index].T) / network.batch_size
    network.squared_gradients[index] = network.squared_gradients[index] + np.square(gradient)
    update = network.eta / (np.sqrt(network.squared_gradients[index] + network.epsilon)) * gradient
    update_bias = network.eta * np.reshape(np.sum(delta, axis=1), network.biases[index].shape)
    return update, update_bias


def adadelta(network, delta, index):
    gradient = (delta @ network.activations[-2 if index == -1 else index].T)
    network.squared_gradients[index] = network.squared_gradients[index] + np.square(gradient)
    rms_delta = np.sqrt(network.squared_updates[index] / network.adadelta_helper + network.epsilon)
    rms_gradient = np.sqrt(network.squared_gradients[index] / network.adadelta_helper + network.epsilon)
    factor = rms_delta / rms_gradient if network.adadelta_helper == 1 else network.eta
    update = factor * gradient
    network.squared_updates[index] = network.squared_updates[index] + np.square(update)
    update_bias = network.eta * np.reshape(np.sum(delta, axis=1), network.biases[index].shape)
    return update, update_bias


def adam(network, delta, index):
    gradient = (delta @ network.activations[-2 if index == -1 else index].T) / network.batch_size
    network.m[index] = network.beta1 * network.m[index] + (1 - network.beta1) * gradient
    network.v[index] = network.beta2 * network.v[index] + (1 - network.beta2) * np.square(gradient)
    m_hat = network.m[index] / (1 - network.beta1)
    v_hat = network.v[index] / (1 - network.beta2)
    update = network.eta * m_hat / (np.sqrt(v_hat) + network.epsilon)
    update_bias = network.eta * np.reshape(np.sum(delta, axis=1), network.biases[index].shape)
    return update, update_bias

