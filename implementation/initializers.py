import numpy as np


def random(size1, size2):
    return np.random.randn(size1, size2)


def zeros(size1, size2):
    return np.zeros((size1, size2))


def he(size1, size2):
    return np.random.randn(size1, size2) * np.sqrt(2 / size2)


def xavier(size1, size2):
    return np.random.randn(size1, size2) * np.sqrt(1 / size2)
